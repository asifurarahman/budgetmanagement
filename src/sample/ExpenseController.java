package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class ExpenseController {

    private FXMLLoader fxmlLoader;
    private Parent root;

    @FXML
    private TextField userExpense;

    @FXML
    private Button buttonExpenseOk;

    @FXML
    void handleExpenseAdd(ActionEvent event) {

        if(!userExpense.getText().isEmpty()) {
            TotalExpenses.Expenses = TotalExpenses.Expenses + Double.parseDouble(userExpense.getText());
            loadAlert();
        }

    }


    private void loadAlert(){

        try{

            fxmlLoader = new FXMLLoader(getClass().getResource("alert.fxml"));
            root = fxmlLoader.load();
            Stages.alertStage = new Stage();
            Stages.alertStage.setScene(new Scene(root));
            Stages.alertStage.initOwner(Stages.expenseStage);
            Stages.alertStage.initModality(Modality.WINDOW_MODAL);
            Stages.alertStage.show();

        }catch (IOException e){
            e.printStackTrace();
        }


    }

}
