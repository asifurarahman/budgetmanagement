package sample;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;

public class Controller {

    private FXMLLoader fxmlLoader;
    private Parent root;
    private TextField textField;

    @FXML
    private TextField userBudget;


    @FXML
    private Button budgetOK;

    @FXML
    void handleBudgetOK(ActionEvent event) {

        if(!userBudget.getText().isEmpty()) {
            Store.userBudget = Double.parseDouble(userBudget.getText());
            loadExpense();
        }


    }

    private void loadExpense(){

        try {

            fxmlLoader = new FXMLLoader(getClass().getResource("expense.fxml"));
            root = (Parent) fxmlLoader.load();
            if(Stages.expenseStage == null) {
                Stages.expenseStage = new Stage();
                Stages.expenseStage.setOnHiding(new EventHandler<WindowEvent>() {
                    @Override
                    public void handle(WindowEvent windowEvent) {
                        Stages.expenseStage = null;
                    }
                });
            }
            if(Stages.resultStage != null){

                Stages.resultStage.close();
                Stages.resultStage = null;

            }
            if(Stages.alertStage != null){

                Stages.alertStage.close();
                Stages.alertStage = null;

            }
            Stages.expenseStage.setScene(new Scene(root));
            getnames();
            initiate();
            Stages.expenseStage.show();

        }catch (IOException i){

            i.printStackTrace();

        }


    }

    private void getnames(){

        textField = (TextField) fxmlLoader.getNamespace().get("userExpense");

    }

    private void initiate(){

        textField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String oldValue, String newValue) {

                if (!newValue.matches("\\d{0,7}([\\.]\\d{0,4})?")) {
                    textField.setText(oldValue);
                }

            }
        });

        textField.requestFocus();
        TotalExpenses.Expenses = 0.0;

    }

}
