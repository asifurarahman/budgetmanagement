package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;

public class AlertController {

    private FXMLLoader fxmlLoader;
    private Parent root;
    private Label label;

    @FXML
    private Button expenseYes;

    @FXML
    private Button expenseNo;

    @FXML
    void handleNo(ActionEvent event) throws IOException {

        Stages.alertStage.close();
        Stages.alertStage = null;
        Stages.expenseStage.close();
        Stages.expenseStage = null;

        loadResult();


    }

    @FXML
    void handleYes(ActionEvent event) {

        Stages.alertStage.close();
        Stages.alertStage = null;

    }

    private void loadResult() throws IOException {

        fxmlLoader = new FXMLLoader(getClass().getResource("result.fxml"));
        root = fxmlLoader.load();
        Stages.resultStage = new Stage();
        Stages.resultStage.setScene(new Scene(root));
        getNames();
        initiate();
        Stages.resultStage.show();

    }

    private void getNames() {

        label = (Label) fxmlLoader.getNamespace().get("resultLabel");

    }

    private void initiate(){

        String result = "";

        if(TotalExpenses.Expenses < Store.userBudget)
            result = "You were under budget " + (Store.userBudget - TotalExpenses.Expenses);
        else if(TotalExpenses.Expenses > Store.userBudget)
            result = "You were over budget " + (TotalExpenses.Expenses - Store.userBudget);
        else
            result = "You used exactly your monthly budget";

        label.setText(result);

    }

}
