package sample;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class Main extends Application {

    private Parent root;
    private FXMLLoader loader;
    private TextField textField;

    @Override
    public void start(Stage primaryStage) throws Exception{
        loader = new FXMLLoader(getClass().getResource("sample.fxml"));
        root = loader.load();
        primaryStage.setTitle("Budget");
        primaryStage.setScene(new Scene(root));
        getnames();
        initiate();
        primaryStage.show();
    }


    private void getnames(){

        textField = (TextField) loader.getNamespace().get("userBudget");

    }

    private void initiate(){

        textField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String oldValue, String newValue) {

                if (!newValue.matches("\\d{0,7}([\\.]\\d{0,4})?")) {
                    textField.setText(oldValue);
                }

            }
        });

        textField.requestFocus();

    }

    public static void main(String[] args) {
        launch(args);
    }
}
